<?php

/**
 * @file
 * Provides OAuth testing functionality.
 */

/**
 * Implements hook_menu().
 */
function oauth_playground_menu() {
  $items = array();
  $items['admin/config/development/oauth_playground'] = array(
    'title' => 'OAuth Playground',
    'description' => 'Developer tools for testing OAuth.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oauth_playground_form'),
    'access arguments' => array('access oauth playground'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function oauth_playground_permission() {
  return array(
    'access oauth playground' => array(
      'description' => t('Test OAuth.'),
      'title' => t('Access OAuth playground'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Main testing form body.
 */
function oauth_playground_form($form, $form_state) {
  $form['url'] = array(
    '#title' => t('URL of OAuth provider'),
    '#description' => t('Please do not use trailing slash and put full URL with the protocol definition, ie. http://provider.com.'),
    '#type' => 'textfield',
    '#default_value' => '',
  );
  $form['key'] = array(
    '#title' => t('OAuth consumer key'),
    '#description' => t('Your key for the provider.'),
    '#type' => 'textfield',
    '#default_value' => '',
  );
  $form['secret'] = array(
    '#title' => t('OAuth consumer secret'),
    '#description' => t('Your secret for the provider.'),
    '#type' => 'textfield',
    '#default_value' => '',
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $sign_methods = array('HMAC-SHA1' => 'HMAC-SHA1');
  foreach (hash_algos() as $algorithm) {
    $sign_methods['HMAC-' . strtoupper($algorithm)] = 'HMAC-' . strtoupper($algorithm);
  }
  $sign_methods['PLAINTEXT'] = 'PLAINTEXT';

  $form['advanced']['sign_method'] = array(
    '#title' => t('Signature method'),
    '#type' => 'select',
    '#options' => $sign_methods,
    '#default_value' => 'HMAC-SHA1',
  );
  $form['advanced']['request_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Request token endpoint'),
    '#description' => t('Absolute path or path relative to base URL.'),
    '#size' => 32,
    '#maxlength' => 255,
    '#default_value' => '/oauth/request_token',
  );
  $form['advanced']['auth_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorization endpoint'),
    '#description' => t('Absolute path or path relative to base URL.'),
    '#size' => 32,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => '/oauth/authorize',
  );
  $form['advanced']['access_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token endpoint'),
    '#description' => t('Absolute path or path relative to base URL.'),
    '#size' => 32,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => '/oauth/access_token',
  );

  $form['submit'] = array(
    '#value' => t('Authenticate'),
    '#type' => 'submit',
  );

  return $form;
}

function oauth_playground_form_validate($form, &$form_state) {
}

function oauth_playground_form_submit($form, &$form_state) {
  global $user;

  $provider = $form_state['values'];

  $callback_url = url('oauth/authorized', array('absolute' => TRUE));
  $consumer = new DrupalOAuthConsumer($provider['key'], $provider['secret'], $callback_url);
  $sig_method = DrupalOAuthClient::signatureMethod(substr(strtolower($provider['advanced']['sign_method']), 5));

  $client = new DrupalOAuthClient($consumer, NULL, $sig_method);

  $request_token = $client->getRequestToken(
    $provider['url'] . $provider['advanced']['request_endpoint'],
    array(
      'realm' => '',
      'callback' => $callback_url,
    )
  );

  $request_token->write();

  $auth_url = $client->getAuthorizationUrl(
    $provider['url'] . $provider['advanced']['auth_endpoint'],
    array(
      'callback' => $callback_url,
    )
  );

  $_SESSION['oauth_playground_request_key'] = $request_token->key;

  if (isset($_GET['destination'])) {
    $destination = $_GET['destination'];
    unset($_GET['destination']);
  }
  elseif (isset($_REQUEST['edit']['destination'])) {
    $destination = $_REQUEST['edit']['destination'];
    unset($_REQUEST['edit']['destination']);
  }
  else {
    $destination = isset($_GET['q']) ? $_GET['q'] : '';
    $query = drupal_http_build_query(drupal_get_query_parameters($_GET, array('q')));
    if ($query != '') {
      $destination .= '?' . $query;
    }
  }

  $_SESSION['oauth_playground_destination'] = $destination;

  drupal_goto($auth_url);
}
